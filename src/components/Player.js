import React, { useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlay,
  faAngleLeft,
  faAngleRight,
  faPause,
} from "@fortawesome/free-solid-svg-icons";

const Player = ({
  currentSong,
  isPlaying,
  setIsPlaying,
  songs,
  setCurrentSong,
  setSongs,
}) => {
  // Refs
  const audioRef = useRef(null);

  // State
  const [songInfo, setSongInfo] = useState({
    currentTime: 0,
    duration: 0,
    animationPercentage: 0,
  });

  const formatTime = (time) => {
    return (
      Math.floor(time / 60) + ":" + ("0" + Math.floor(time % 60)).slice(-2)
    );
  };

  // Event handlers
  const activeLibraryHandler = (nextPrevious) => {
    // Update the song status
    setSongs(
      songs.map((s) => {
        return {
          ...s,
          active: s.id === nextPrevious.id,
        };
      })
    );
  };

  const playsSongHandler = () => {
    !isPlaying ? audioRef.current.play() : audioRef.current.pause();

    setIsPlaying(!isPlaying);
  };

  const timeUpdateHandler = (e) => {
    const currentTime = e.target.currentTime;
    const duration = e.target.duration;

    // Calaculate song percentage
    const roundedCurrentTime = Math.round(currentTime);
    const roundedDurationTime = Math.round(duration);
    const animationPercentage = Math.round(
      (roundedCurrentTime / roundedDurationTime) * 100
    );

    setSongInfo({
      ...songInfo,
      currentTime,
      duration,
      animationPercentage,
    });
  };

  const dragHandler = (e) => {
    const value = e.target.value;

    audioRef.current.currentTime = value;

    setSongInfo({
      ...songInfo,
      currentTime: value,
    });
  };

  const autoPlayHandler = () => {
    if (isPlaying) {
      audioRef.current.play();
    }
  };

  const skipTrackHandler = (direction) => {
    const songsCount = songs.length;
    let currentIndex = songs.findIndex((song) => song.id === currentSong.id);

    if (direction === "skip-forward") {
      let nextSong = songs[(currentIndex + 1) % songsCount];

      setCurrentSong(nextSong);
      activeLibraryHandler(nextSong);
    }

    if (direction === "skip-back") {
      if ((currentIndex - 1) % songsCount === -1) {
        let firstSong = songs[songsCount - 1];

        setCurrentSong(firstSong);
        activeLibraryHandler(firstSong);
        return;
      }

      let previousSong = songs[(currentIndex - 1) % songsCount];
      setCurrentSong(previousSong);
      activeLibraryHandler(previousSong);
    }
  };

  const songEndedHandler = () => {
    skipTrackHandler("skip-forward");
  };

  // Add styles
  const trackAnimationStyle = {
    transform: `translateX(${songInfo.animationPercentage}%)`,
  };

  const trackStyle = {
    background: `linear-gradient(to right, ${currentSong.color[0]}, ${currentSong.color[1]})`,
  };

  return (
    <div className="player">
      <div className="time-control">
        <p>{formatTime(songInfo.currentTime)}</p>
        <div className="track" style={trackStyle}>
          <input
            type="range"
            min={0}
            max={songInfo.duration || 0}
            value={songInfo.currentTime}
            onChange={dragHandler}
          />
          <div className="animate-track" style={trackAnimationStyle}></div>
        </div>
        <p>
          {songInfo.duration
            ? formatTime(songInfo.duration - songInfo.currentTime)
            : "0.00"}
        </p>
      </div>
      <div className="play-control">
        <FontAwesomeIcon
          className="skip-back"
          onClick={() => skipTrackHandler("skip-back")}
          icon={faAngleLeft}
          size="2x"
        />
        <FontAwesomeIcon
          className="play"
          icon={!isPlaying ? faPlay : faPause}
          size="2x"
          onClick={playsSongHandler}
        />
        <FontAwesomeIcon
          className="skip-forwared"
          onClick={() => skipTrackHandler("skip-forward")}
          icon={faAngleRight}
          size="2x"
        />
        <audio
          ref={audioRef}
          src={currentSong.audio}
          onTimeUpdate={timeUpdateHandler}
          onLoadedMetadata={timeUpdateHandler}
          onLoadedData={autoPlayHandler}
          onEnded={songEndedHandler}
        ></audio>
      </div>
    </div>
  );
};

export default Player;
